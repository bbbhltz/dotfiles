---
title: title
subtitle: subtitle
date: 2022
institute: SKALA
subject: English
author: bbbhltz
titlegraphic: assets/logo.png
lang: en-GB
colortheme: whale
outertheme: infolines
innertheme: rectangles
mainfont: Public Sans
mathfont: FreeSerif
monofont: Hack
fonttheme: structurebold
toc: true
linkcolor: white
# filecolor:
citecolor: sgreen
toccolor: sgreen
urlcolor: sgreen
---

# Title Slide

## Heading

\renewmenumacro{\directory}{pathswithfolder}

\directory{home/bbbhltz/Documents/}

\menu{home > bbbhltz > Documents}

\keys{\ctrl + \Alt + \backdel}

### Block
Content $2.50 or €1.29
