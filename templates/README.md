# Templates

## CV Template

**IN PROGRESS**

*trying to muck around with LaTeX a little more*

### CV Example

![](CV/CV-English.v2-1.png)

## Beamer Templates

These "Template" files are meant to be used with [pandoc](https://pandoc.org/) to convert a [Markdown](https://daringfireball.net/projects/markdown/) file to a [Beamer presentation](https://ctan.org/pkg/beamer).

The templates both use similar base-themes with very minor colour changes to match the schools I work for:

```yaml
colortheme: whale
outertheme: infolines
innertheme: rectangles
mainfont: fontname
monofont: fontname
```

Eventually, I will try and make something more custom. Most of the extra packages use default functions available in official documentation.

The command that works well with these templates is `pandoc --pdf-engine=xelatex --include-in-header=style.tex -t beamer INPUT.md -o OUTPUT.md`.

### Beamer Examples

<table>
<thead>
  <tr>
    <th class="tg-0pky">NEOMA</th>
    <th class="tg-0pky">ISCOM</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky"><img src=NEOMA/NEOMA-1.png></td>
    <td class="tg-0pky"><img src=ISCOM/ISCOM-1.png></td>
  </tr>
  <tr>
    <td class="tg-0pky"><img src=NEOMA/NEOMA-2.png></td>
    <td class="tg-0pky"><img src=ISCOM/ISCOM-2.png></td>
  </tr>
  <tr>
    <td class="tg-0pky"><img src=NEOMA/NEOMA-3.png></td>
    <td class="tg-0pky"><img src=ISCOM/ISCOM-3.png></td>
  </tr>
  <tr>
    <td class="tg-0lax"><img src=NEOMA/NEOMA-4.png></td>
    <td class="tg-0lax"><img src=ISCOM/ISCOM-4.png></td>
  </tr>
  <tr>
    <td class="tg-0lax"><img src=NEOMA/NEOMA-5.png></td>
    <td class="tg-0lax"><img src=ISCOM/ISCOM-5.png></td>
  </tr>
  <tr>
    <td class="tg-0lax"><img src=NEOMA/NEOMA-6.png></td>
    <td class="tg-0lax"><img src=ISCOM/ISCOM-6.png></td>
  </tr>
</tbody>
</table>
