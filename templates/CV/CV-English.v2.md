---
documentclass: letter
lang: en-GB
geometry:
- margin=1.5cm
mainfont: FreeSans
fontsize: 11pt
papersize: A4
header-includes:
    - \usepackage{multicol, tcolorbox}
    - \tcbuselibrary{skins}
    - \newcommand{\xp}[5]{\textbf{#1}\hfill\textbf{#2} \\ \textit{#3}\hfill\textit{#4} \ \textendash \ \textit{#5}}
    - \newcommand{\hideFromPandoc}[1]{#1}
    - \hideFromPandoc{
        \let\Begin\begin
        \let\End\end
      }
---

\pagestyle{empty}

\begin{multicols}{2}

\begin{huge}

\textbf{FIRST LAST}

\end{huge}

Title

\vfill\null

\columnbreak

\Begin{flushright}

Address 1 \\
Address 2

phone number \\
email address \\
website or linkedin

\End{flushright}

\end{multicols}

\begin{tcolorbox}[colback=white,sharp corners,colframe=black,title=Experience,fonttitle=\Large\bfseries]

\xp{A Company}{A City}{A Job Title}{JAN 2020}{SEP 2021}

Details \\

\xp{A Company}{A City}{A Job Title}{JAN 2020}{SEP 2021}

Details

\tcblower

\begin{small}\color{gray}

\xp{Old Company}{Old City}{Old Job Title}{MAY 1999}{OCT 2020}

Did some stuff \\

\xp{Old Company}{Old City}{Old Job Title}{MAY 1999}{OCT 2020}

Did some stuff \\

\xp{Old Company}{Old City}{Old Job Title}{MAY 1999}{OCT 2020}

Did some stuff

\end{small}

\end{tcolorbox}

\begin{tcolorbox}[colback=white,sharp corners,colframe=black,title=Education,fonttitle=\Large\bfseries]

\xp{Univeristy of X}{Shitty Town}{Expensive Degree}{2000}{2004}

\end{tcolorbox}

\begin{tcolorbox}[colback=white,sharp corners,colframe=black,title=Skills,fonttitle=\Large\bfseries]

\textbf{Language}:

\textbf{Computer}:

\end{tcolorbox}
