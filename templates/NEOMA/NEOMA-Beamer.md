---
title: Title
subtitle: Subtitle
date: 2023
institute: NEOMA Business School
subject: English
author: English
titlegraphic: assets/titleneoma.png
lang: en-GB
colortheme: whale
outertheme: infolines
innertheme: rectangles
mainfont: FreeSans
monofont: Hack
include-in-header: style.tex
pdf-engine: xelatex
---

# Title Slide

## Heading

\renewmenumacro{\directory}{pathswithfolder}

\directory{home/bbbhltz/Documents/}

\menu{home > bbbhltz > Documents}

\keys{\ctrl + \Alt + \backdel}

### Block
Content $2.50 or €1.29

---

Empty

## Cool Colors

\xmybox{WORD!}

## Quote

\begin{quotebox}{Some person}

Quote quote \uwave{quote}

\end{quotebox}

## Code

```
print("Hello World")
```
