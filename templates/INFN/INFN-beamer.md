---
title: Title
subtitle: Subtitle
date: 2021
institute: ISCOM
subject: English
author: English
titlegraphic: assets/logo.png
lang: en-GB
colortheme: whale
outertheme: infolines
innertheme: rectangles
mainfont: Carlito
monofont: Hack
fonttheme: structurebold
---

# Title Slide

## Heading

\renewmenumacro{\directory}{pathswithfolder}

\directory{home/bbbhltz/Documents/}

\menu{home > bbbhltz > Documents}

\keys{\ctrl + \Alt + \backdel}

### Block
Content $2.50 or €1.29

---

Empty

## Cool Colors

\xmybox{WORD!}

## Quote

\begin{quotebox}{Some person}

Quote quote \uwave{quote}

\end{quotebox}
