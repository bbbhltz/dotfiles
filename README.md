# Personal Settings

Here are my dotfiles and templates.

Having lost settings in the past, I have decided to keep these several files here.

## dotfiles

* beets
* newsboat
* rofi
* mpv

## templates

*these are for my work*

* markdown to beamer template files
* beamer styles
