# Personal Settings

Here are my dotfiles and templates.

Having lost settings in the past, I have decided to keep these several files here.

## dotfiles

* beets
* newsboat
* rofi
* mpv
* qutebrowser
* tut
* etc.

## templates

*these are for my work*

* markdown to beamer template files
* beamer styles

## Notes

This repository is maintained on Codeberg.

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)
