# QUTEBROWSER CONFIGURATION
# BBBHLTZ

# test

# c.qt.force_platformtheme = 'gtk3'

# GENERAL

config.load_autoconfig(False)
c.url.start_pages = 'https://start.duckduckgo.com/'
c.colors.webpage.darkmode.enabled = False
c.content.autoplay = False
c.content.pdfjs = False
c.completion.web_history.max_items = 100
c.downloads.location.directory = '~/Downloads'
c.downloads.location.prompt = False
c.downloads.remove_finished = -1
# c.editor.command = ['qterminal', '-e' 'micro', '{file}']
c.editor.command = ['xed', '{file}']
c.editor.encoding = 'utf-8'
c.scrolling.smooth = False
c.spellcheck.languages = ['en-GB', 'fr-FR']
c.content.javascript.clipboard = 'access-paste'
c.zoom.default = '130%'

# config.set('qt.args', ['enable-experimental-web-platform-features'])

# SEARCH ENGINES

c.url.searchengines = {
    'DEFAULT': 'https://duckduckgo.com/?q={}',
    'k': 'https://kagi.com/search?token=HAAAJm4ySgI.5W2qUzqsLqaRzATyFuYL1I1M5jYKznn0a6wiMKZFfdw&q={}',
    'w': 'https://en.wikipedia.org/?search={}',
    'wr': 'https://www.wordreference.com/enfr/{}',
    'moj': 'https://www.mojeek.com/search?q={}',
    't': 'https://teddit.net/r/all/search?q={}&nsfw=on',
    'mb': 'https://musicbrainz.org/search?query={}&type=artist&method=indexed',
    'conj': 'https://www.wordreference.com/conj/frverbs.aspx?v={}',
    'discogs': 'https://www.discogs.com/search/?q={}&type=all',
    'yt': 'https://invidious.fdn.fr/search?q={}',
    'bc': 'https://bandcamp.com/search?q={}',
    'osm': 'https://www.openstreetmap.org/search?query={}'
}

# BLOCKING / ADS
# "https://secure.fanboy.co.nz/fanboy-cookiemonster.txt"
# "https://secure.fanboy.co.nz/fanboy-annoyance.txt",
# HOSTS
# https://raw.githubusercontent.com/AdroitAdorKhan/antipopads-re/master/formats/hosts.txt
# "https://pgl.yoyo.org/as/serverlist.php?showintro=0;hostformat=hosts",
# "http://sbc.io/hosts/alternates/fakenews-gambling-porn/hosts",

c.content.blocking.enabled = True
c.content.blocking.hosts.block_subdomains = True
c.content.blocking.method = 'both' # Can be 'adblock' 'auto' 'both'
c.content.blocking.whitelist = [
    'https://www.proquest.com/*',
    '*://consent.trustarc.com/*',
    '*://*.neoma-bs.fr/*',
    '*://*.norm-uni.fr/*',
    '*://*.nintendo.fr/*'
    ]
c.content.blocking.adblock.lists = [
    "https://easylist.to/easylist/easylist.txt",
    "https://easylist.to/easylist/easyprivacy.txt",
    "https://easylist.to/easylist/fanboy-social.txt",
    "https://easylist-downloads.adblockplus.org/liste_fr.txt",
    ]
c.content.blocking.hosts.lists = [
    "https://raw.githubusercontent.com/d3ward/toolz/master/src/d3host.txt",
    "https://someonewhocares.org/hosts/hosts",
    "https://codeberg.org/bbbhltz/dotfiles/raw/branch/main/blocklists/reworld.txt",
    "https://sebsauvage.net/hosts/hosts"
    ]

# FONTS

c.fonts.default_family = 'Noto Sans'
# c.fonts.web.family.sans_serif = 'DejaVu Sans'
# c.fonts.web.family.serif = 'DejaVu Serif'
# c.fonts.web.family.standard = 'DejaVu Sans'
# c.fonts.web.family.cursive = ''
# c.fonts.web.family.fantasy = 'Comic Neue'
c.fonts.default_size = '15pt'
# c.fonts.web.family.fixed = 'Hack'
# c.fonts.web.size.default = 18
# c.fonts.web.size.default_fixed = 18
# c.fonts.web.size.minimum = 15
# c.fonts.web.size.minimum_logical = 8
# c.qt.highdpi = True

# Site Specific

config.set('content.register_protocol_handler', True, 'https://outlook.office365.com')
config.set('content.register_protocol_handler', True, 'https://*.norm-uni.fr')
config.set('content.register_protocol_handler', True, 'https://learning.edx.org')
config.set('content.register_protocol_handler', True, 'https://*.github.dev')
config.set('content.register_protocol_handler', True, 'https://*.neoma-bs.fr')
config.set('content.register_protocol_handler', True, 'https://*.edx.org')
config.set('content.register_protocol_handler', True, 'https://*.cs50.io')
config.set('content.register_protocol_handler', True, 'https://*.darty.com')
config.set('content.register_protocol_handler', True, 'https://*.sncf-connect.com')
config.set('content.register_protocol_handler', True, 'https://*.omio.fr')
config.set('content.register_protocol_handler', True, 'https://*.openstreetmap.org')
config.set('content.register_protocol_handler', True, 'https://*.caisse-epargne.fr')
config.set('content.register_protocol_handler', True, 'https://*.cnil.fr')
config.set('content.register_protocol_handler', True, 'https://*.urkund.com')
config.set('content.register_protocol_handler', True, 'https://*.mailbox.org')
config.set('content.register_protocol_handler', True, 'https://*.doctolib.fr')
config.set('content.register_protocol_handler', True, 'https://*.microsoft.com')
config.set('content.register_protocol_handler', True, 'https://*.seine-maritime.gouv.fr')
config.set('content.register_protocol_handler', True, 'https://*.openstreetmap.org')
config.set('content.register_protocol_handler', True, 'https://*.openstreetmap.fr')
config.set('content.blocking.enabled', False, '*://*.doctolib.fr/*')
config.set('content.blocking.enabled', False, '*://*.norm-uni.fr/*')
config.set('content.blocking.enabled', False, '*://*.microsoft.com/*')
config.set('content.blocking.enabled', False, '*://*.proquest.com/*')
config.set('content.blocking.enabled', False, '*://*.mojeek.com/*')
config.set('content.blocking.enabled', False, '*://*.openstreetmap.org/*')
config.set('content.blocking.enabled', False, '*://*.openstreetmap.fr/*')
config.set('content.blocking.enabled', False, '*://*.neoma-bs.fr/*')
config.set('content.blocking.enabled', False, '*://*.neoma-bs.com/*')
config.set('content.blocking.enabled', False, '*://*.fnac.com/*')
config.set('content.blocking.enabled', False, '*://*.darty.com/*')
config.set('content.blocking.enabled', False, '*://*.dailymotion.com/*')
config.set('content.blocking.enabled', False, '*://*.linkedin.com/*')
config.set('content.blocking.enabled', False, '*://*.microsoft.com/*')
config.set('content.blocking.enabled', False, '*://*.edx.org/*')
config.set('content.blocking.enabled', False, '*://*.lego.com/*')
config.set('content.blocking.enabled', False, '*://*.lego.ca/*')
config.set('content.blocking.enabled', False, '*://*.lego.fr/*')
config.set('content.blocking.enabled', False, '*://*.cs50.io/*')
config.set('content.blocking.enabled', False, '*://*.sncf-connect.com/*')
config.set('content.blocking.enabled', False, '*://*.caisse-epargne.fr/*')
config.set('content.blocking.enabled', False, '*://*.net142.caisse-epargne.fr/*')
config.set('content.blocking.enabled', False, '*://*.laposte.fr/*')
config.set('content.blocking.enabled', False, '*://*.cnil.fr/*')
config.set('content.blocking.enabled', False, '*://*.urkund.com/*')
config.set('content.blocking.enabled', False, '*://*.mailbox.org/*')
config.set('content.blocking.enabled', False, '*://*.nintendo.fr/*')
config.set('fonts.web.size.minimum', 15, '*://*.fosstodon.org/*')
config.set('fonts.web.size.minimum', 15, 'https://news.ycombinator.com/*')
config.set('fonts.web.size.minimum', 15, 'https://lobste.rs/*')

config.set('content.notifications.enabled', True, 'https://beehaw.org')

# BINDINGS

config.bind('<Ctrl+Shift+l>', 'spawn --userscript qute-bitwarden')
config.bind('ar', 'spawn --userscript readability')
config.bind('gF', 'spawn --userscript openfeeds')
config.bind('sn', 'spawn --userscript linkcollector {title} {url}')
config.bind('sp', 'spawn /home/bbbhltz/.local/bin/wallabag add {url}')
config.bind('ya', 'yank inline {title}" "{url:pretty}')
config.bind('sa', 'open -t https://archive.md/{url}')

# NORD

nord = {
    # Polar Night
    'nord0': '#2e3440',
    'nord1': '#3b4252',
    'nord2': '#434c5e',
    'nord3': '#4c566a',
    # Snow Storm
    'nord4': '#d8dee9',
    'nord5': '#e5e9f0',
    'nord6': '#eceff4',
    # Frost
    'nord7': '#8fbcbb',
    'nord8': '#88c0d0',
    'nord9': '#81a1c1',
    'nord10': '#5e81ac',
    # Aurora
    'nord11': '#bf616a',
    'nord12': '#d08770',
    'nord13': '#ebcb8b',
    'nord14': '#a3be8c',
    'nord15': '#b48ead',
}

## Background color of the completion widget category headers.
## Type: QssColor
c.colors.completion.category.bg = nord['nord0']

## Bottom border color of the completion widget category headers.
## Type: QssColor
c.colors.completion.category.border.bottom = nord['nord0']

## Top border color of the completion widget category headers.
## Type: QssColor
c.colors.completion.category.border.top = nord['nord0']

## Foreground color of completion widget category headers.
## Type: QtColor
c.colors.completion.category.fg = nord['nord5']

## Background color of the completion widget for even rows.
## Type: QssColor
c.colors.completion.even.bg = nord['nord1']

## Background color of the completion widget for odd rows.
## Type: QssColor
c.colors.completion.odd.bg = nord['nord1']

## Text color of the completion widget.
## Type: QtColor
c.colors.completion.fg = nord['nord4']

## Background color of the selected completion item.
## Type: QssColor
c.colors.completion.item.selected.bg = nord['nord3']

## Bottom border color of the selected completion item.
## Type: QssColor
c.colors.completion.item.selected.border.bottom = nord['nord3']

## Top border color of the completion widget category headers.
## Type: QssColor
c.colors.completion.item.selected.border.top = nord['nord3']

## Foreground color of the selected completion item.
## Type: QtColor
c.colors.completion.item.selected.fg = nord['nord6']

## Foreground color of the matched text in the completion.
## Type: QssColor
c.colors.completion.match.fg = nord['nord13']

## Color of the scrollbar in completion view
## Type: QssColor
c.colors.completion.scrollbar.bg = nord['nord1']

## Color of the scrollbar handle in completion view.
## Type: QssColor
c.colors.completion.scrollbar.fg = nord['nord5']

## Background color for the download bar.
## Type: QssColor
c.colors.downloads.bar.bg = nord['nord0']

## Background color for downloads with errors.
## Type: QtColor
c.colors.downloads.error.bg = nord['nord11']

## Foreground color for downloads with errors.
## Type: QtColor
c.colors.downloads.error.fg = nord['nord5']

## Color gradient stop for download backgrounds.
## Type: QtColor
c.colors.downloads.stop.bg = nord['nord15']

## Color gradient interpolation system for download backgrounds.
## Type: ColorSystem
## Valid values:
##   - rgb: Interpolate in the RGB color system.
##   - hsv: Interpolate in the HSV color system.
##   - hsl: Interpolate in the HSL color system.
##   - none: Don't show a gradient.
c.colors.downloads.system.bg = 'none'

## Background color for hints. Note that you can use a `rgba(...)` value
## for transparency.
## Type: QssColor
c.colors.hints.bg = nord['nord13']

## Font color for hints.
## Type: QssColor
c.colors.hints.fg = nord['nord0']

## Font color for the matched part of hints.
## Type: QssColor
c.colors.hints.match.fg = nord['nord10']

## Background color of the keyhint widget.
## Type: QssColor
c.colors.keyhint.bg = nord['nord1']

## Text color for the keyhint widget.
## Type: QssColor
c.colors.keyhint.fg = nord['nord5']

## Highlight color for keys to complete the current keychain.
## Type: QssColor
c.colors.keyhint.suffix.fg = nord['nord13']

## Background color of an error message.
## Type: QssColor
c.colors.messages.error.bg = nord['nord11']

## Border color of an error message.
## Type: QssColor
c.colors.messages.error.border = nord['nord11']

## Foreground color of an error message.
## Type: QssColor
c.colors.messages.error.fg = nord['nord5']

## Background color of an info message.
## Type: QssColor
c.colors.messages.info.bg = nord['nord8']

## Border color of an info message.
## Type: QssColor
c.colors.messages.info.border = nord['nord8']

## Foreground color an info message.
## Type: QssColor
c.colors.messages.info.fg = nord['nord5']

## Background color of a warning message.
## Type: QssColor
c.colors.messages.warning.bg = nord['nord12']

## Border color of a warning message.
## Type: QssColor
c.colors.messages.warning.border = nord['nord12']

## Foreground color a warning message.
## Type: QssColor
c.colors.messages.warning.fg = nord['nord5']

## Background color for prompts.
## Type: QssColor
c.colors.prompts.bg = nord['nord2']

# ## Border used around UI elements in prompts.
# ## Type: String
c.colors.prompts.border = '1px solid ' + nord['nord0']

## Foreground color for prompts.
## Type: QssColor
c.colors.prompts.fg = nord['nord5']

## Background color for the selected item in filename prompts.
## Type: QssColor
c.colors.prompts.selected.bg = nord['nord3']

## Background color of the statusbar in caret mode.
## Type: QssColor
c.colors.statusbar.caret.bg = nord['nord15']

## Foreground color of the statusbar in caret mode.
## Type: QssColor
c.colors.statusbar.caret.fg = nord['nord5']

## Background color of the statusbar in caret mode with a selection.
## Type: QssColor
c.colors.statusbar.caret.selection.bg = nord['nord15']

## Foreground color of the statusbar in caret mode with a selection.
## Type: QssColor
c.colors.statusbar.caret.selection.fg = nord['nord5']

## Background color of the statusbar in command mode.
## Type: QssColor
c.colors.statusbar.command.bg = nord['nord2']

## Foreground color of the statusbar in command mode.
## Type: QssColor
c.colors.statusbar.command.fg = nord['nord5']

## Background color of the statusbar in private browsing + command mode.
## Type: QssColor
c.colors.statusbar.command.private.bg = nord['nord2']

## Foreground color of the statusbar in private browsing + command mode.
## Type: QssColor
c.colors.statusbar.command.private.fg = nord['nord5']

## Background color of the statusbar in insert mode.
## Type: QssColor
c.colors.statusbar.insert.bg = nord['nord14']

## Foreground color of the statusbar in insert mode.
## Type: QssColor
c.colors.statusbar.insert.fg = nord['nord1']

## Background color of the statusbar.
## Type: QssColor
c.colors.statusbar.normal.bg = nord['nord0']

## Foreground color of the statusbar.
## Type: QssColor
c.colors.statusbar.normal.fg = nord['nord5']

## Background color of the statusbar in passthrough mode.
## Type: QssColor
c.colors.statusbar.passthrough.bg = nord['nord10']

## Foreground color of the statusbar in passthrough mode.
## Type: QssColor
c.colors.statusbar.passthrough.fg = nord['nord5']

## Background color of the statusbar in private browsing mode.
## Type: QssColor
c.colors.statusbar.private.bg = nord['nord3']

## Foreground color of the statusbar in private browsing mode.
## Type: QssColor
c.colors.statusbar.private.fg = nord['nord5']

## Background color of the progress bar.
## Type: QssColor
c.colors.statusbar.progress.bg = nord['nord5']

## Foreground color of the URL in the statusbar on error.
## Type: QssColor
c.colors.statusbar.url.error.fg = nord['nord11']

## Default foreground color of the URL in the statusbar.
## Type: QssColor
c.colors.statusbar.url.fg = nord['nord5']

## Foreground color of the URL in the statusbar for hovered links.
## Type: QssColor
c.colors.statusbar.url.hover.fg = nord['nord8']

## Foreground color of the URL in the statusbar on successful load
## (http).
## Type: QssColor
c.colors.statusbar.url.success.http.fg = nord['nord5']

## Foreground color of the URL in the statusbar on successful load
## (https).
## Type: QssColor
c.colors.statusbar.url.success.https.fg = nord['nord14']

## Foreground color of the URL in the statusbar when there's a warning.
## Type: QssColor
c.colors.statusbar.url.warn.fg = nord['nord12']

## Background color of the tab bar.
## Type: QtColor
c.colors.tabs.bar.bg = nord['nord3']

## Background color of unselected even tabs.
## Type: QtColor
c.colors.tabs.even.bg = nord['nord3']

## Foreground color of unselected even tabs.
## Type: QtColor
c.colors.tabs.even.fg = nord['nord5']

## Color for the tab indicator on errors.
## Type: QtColor
c.colors.tabs.indicator.error = nord['nord11']

## Color gradient start for the tab indicator.
## Type: QtColor
# c.colors.tabs.indicator.start = nord['violet']

## Color gradient end for the tab indicator.
## Type: QtColor
# c.colors.tabs.indicator.stop = nord['orange']

## Color gradient interpolation system for the tab indicator.
## Type: ColorSystem
## Valid values:
##   - rgb: Interpolate in the RGB color system.
##   - hsv: Interpolate in the HSV color system.
##   - hsl: Interpolate in the HSL color system.
##   - none: Don't show a gradient.
c.colors.tabs.indicator.system = 'none'

## Background color of unselected odd tabs.
## Type: QtColor
c.colors.tabs.odd.bg = nord['nord3']

## Foreground color of unselected odd tabs.
## Type: QtColor
c.colors.tabs.odd.fg = nord['nord5']

# ## Background color of selected even tabs.
# ## Type: QtColor
c.colors.tabs.selected.even.bg = nord['nord0']

# ## Foreground color of selected even tabs.
# ## Type: QtColor
c.colors.tabs.selected.even.fg = nord['nord5']

# ## Background color of selected odd tabs.
# ## Type: QtColor
c.colors.tabs.selected.odd.bg = nord['nord0']

# ## Foreground color of selected odd tabs.
# ## Type: QtColor
c.colors.tabs.selected.odd.fg = nord['nord5']

## Background color for webpages if unset (or empty to use the theme's
## color)
## Type: QtColor
# c.colors.webpage.bg = 'white'
