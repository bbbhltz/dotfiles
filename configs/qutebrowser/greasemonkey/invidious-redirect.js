// ==UserScript==
// @name           YouTube - Invidious Redirect
// @match          http://www.youtube.com/*
// @match          https://www.youtube.com/*
// @match          http://youtube.com/*
// @match          https://youtube.com/*
// @run-at         document-start
// ==/UserScript==

// var invidious='invidious.snopyta.org';
// var invidious='yewtu.be';
// var invidious='invidious.kavin.rocks';
var invidious='inv.odyssey346.dev';

location.host=location.host.replace("www.youtube.com",invidious);
