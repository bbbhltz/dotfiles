// ==UserScript==
// @name         Redirect to CBC Lite
// @author       bbbhltz
// @description  Prefer the lite CBC version
// @version      0.0.1
// @match        *.cbc.ca/*
// @run-at       document-start
// @grant        none
// ==/UserScript==

(function() {
    let currentUrl = location.href;

    function redirect() {
        const pathname = location.pathname;
        if (!pathname.startsWith('/news')) return;

        const storyId = pathname.split('-').pop();

        location.href = `https://www.cbc.ca/lite/story/${storyId}`;
    }

    const observer = new MutationObserver(() => {
        // URL hasn't changed
        if (currentUrl === location.href) return;

        currentUrl = location.href;

        redirect();
    });
