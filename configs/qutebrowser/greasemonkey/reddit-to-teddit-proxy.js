// ==UserScript==
// @name           Reddit to Teddit proxy
// @include        /^http(s|)://(www\.|)reddit\.com/.*$/
// @run-at         document-start
// ==/UserScript==

var instance='teddit.hostux.net';

var url=new URL(window.location.href);        
url.hostname=instance;
location.href=url;
