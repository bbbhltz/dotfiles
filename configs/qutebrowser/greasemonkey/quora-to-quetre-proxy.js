// ==UserScript==
// @name Quora to Quetre Proxy
// @include *quora.com/*
// @run-at document-start
// ==/UserScript==

var url = new URL(location.href),

// INSTANCES //

quetre = 'quetre.iket.me'
// quetre = 'quetre.pussthecat.org'
// quetre = 'quora.vern.cc'
// quetre = 'quetreus.herokuapp.com'
// quetre = 'quetre.tokhmi.xyz'
// quetre = 'quetre.projectsegfau.lt'
// quetre = 'quetre.esmailelbob.xyz'
// quetre = 'quetre.odyssey346.dev'
// quetre = 'quetre.privacydev.net'
// quetre = 'ask.habedieeh.re'
// quetre = 'quetre.marcopisco.com'


// REDIRECT //

if(hostHas('quora.com')) {
    location.replace('https://' + quetre + location.pathname + location.search)
}

function hostHas(str) {
    return location.host.indexOf(str) != -1
}
