original = CSS.supports
CSS.supports = function(x) {
  if (x.startsWith("aspect-ratio"))
    return true
  else
    return original(x)
}
