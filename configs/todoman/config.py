path = "~/.tasks/mailbox/*"
# humanize = True
date_format = "%Y.%m.%d"
time_format = "%H:%M"
default_list = "MzM"
default_due = 48
color = "always"

