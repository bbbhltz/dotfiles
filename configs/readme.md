# configs

## beets

beets is a "media library management system for obsessive music geeks".

I use it to move files around and make sure they are tagged correctly.

For more info: [https://beets.io/](https://beets.io/)

## khal & khard / vdirsyncer

these are configs for syncing my calendar and contacts with `mailbox.org`

## mpv

mpv is a command line media player. I like it because it does what it needs to do, and nothing else.

For more info: [https://mpv.io/](https://mpv.io/)

## newsboat

Newsboat is an RSS/Atom feed reader for the text console.

It is fast and looks good enough for me. My original config file is lost to the ages so I am starting over.

For more info: [https://newsboat.org/](https://newsboat.org/)

## qutebrowser

some of my browser configs, not very interesting... unless you like blocklists?

## rofi

basic rofi config

## tut

Configs for [tut](https://tut.anv.nu/)

Includes my "Breeze Dark" theme.
