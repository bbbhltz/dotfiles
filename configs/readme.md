# configs

## beets

beets is a "media library management system for obsessive music geeks".

I use it to move files around and make sure they are tagged correctly.

For more info: [https://beets.io/](https://beets.io/)

## mpv

mpv is a command line media player. I like it because it does what it needs to do, and nothing else.

For more info: [https://mpv.io/](https://mpv.io/)

## newsboat

Newsboat is an RSS/Atom feed reader for the text console.

It is fast and looks good enough for me. My original config file is lost to the ages so I am starting over.

For more info: [https://newsboat.org/](https://newsboat.org/)

## rofi

Rofi does many things. I have only started using it, so my config is near vanilla.

![Rofi screenshot](rofishot.png)

For more info: [https://github.com/davatorium/rofi](https://github.com/davatorium/rofi)
